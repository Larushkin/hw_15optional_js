 'use strict'
// Теория 
//Рекурсия – это приём программирования, полезен когда одна задача может быть разделена на несколько более простых задач. 
//Или когда мы можем упростить задачу и добавить  простой вариант этой же задачи.




let a = prompt("Введите число");
let n = checkNum(+a) 

function checkNum(num) {
    if (isNaN(+num) || +num == null) {
        while (isNaN(+num) || +num == null) {
            num = prompt('Введите число!', a);
        }
    }
    return(num);
}

function factorial(n) 
{ 

  if (n === 0)
 {
    return 1;
 }
  return n * factorial(n-1);
         
}

alert(` Факториал числа ${n} равен ${factorial(n)}`);